import {ScrollView} from "react-native";
import React from "react";
import {DriverType} from "../../../domain/interfaces/driverType";
import {DriversItem} from "../drivers-item";

export const DriversList = (props: { drivers: DriverType[] }) => {
    return (
        <ScrollView>
            {props.drivers.map(
                driver => <DriversItem key={driver.driverId} driver={driver}/>
            )}
        </ScrollView>
    );
}

