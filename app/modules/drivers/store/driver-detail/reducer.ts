import {DriverType} from "../../domain/interfaces/driverType";
import {
    DriverDetailActionsType,
    SET_DRIVER_DETAIL
} from "./actions";

export const driverDetailInitialState = {
    driver: {} as DriverType,
}

type DriverDetailInitialStateType = typeof driverDetailInitialState

export const driverDetailReducer = (
    state = driverDetailInitialState, action: DriverDetailActionsType
): DriverDetailInitialStateType => {
    switch (action.type) {
        case SET_DRIVER_DETAIL:
            return {
                ...state,
                driver: action.driver
            }
        default:
            return state
    }
};