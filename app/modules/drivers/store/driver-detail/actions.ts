import {DriverType} from "../../domain/interfaces/driverType";
import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {driversAPI} from "../../domain/API/driversAPI";

export const SET_DRIVER_DETAIL = 'SET_DRIVER_DETAIL';

export type setDriverDetailActionType = {
    type: typeof SET_DRIVER_DETAIL
    driver: DriverType
}

export const setDriverDetail = (driver: DriverType): setDriverDetailActionType => ({type: SET_DRIVER_DETAIL, driver});

export const requestDriverDetail = (id: string) => {
    return async (dispatch: Dispatch<DriverDetailActionsType>, getState: RootStateType) => {
        let data = await driversAPI.getDriver(id)
        dispatch(setDriverDetail(data))
    }
}


export type DriverDetailActionsType = setDriverDetailActionType
