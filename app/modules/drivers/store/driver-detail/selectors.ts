import {createSelector} from "reselect";
import { driversState } from "../selectors";

export const driverDetailStateSelector = createSelector(
    driversState,
    (state) => state.driverDetailReducer
)

export const driverDetailSelector = createSelector(
    driverDetailStateSelector,
    (state) => state.driver
)