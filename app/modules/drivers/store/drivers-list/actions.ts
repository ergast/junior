import {DriverType} from "../../domain/interfaces/driverType";
import {driversAPI} from "../../domain/API/driversAPI";
import {RootStateType} from "../../../store/reducer";
import {Dispatch} from "redux";

export const SET_DRIVERS = 'SET_DRIVERS';
export const SET_DRIVERS_PAGINATION = 'SET_DRIVERS_PAGINATION';


export type DriversPagePagination = {
    offset: number,
    limit: number
}

export type setDriversActionType = {
    type: typeof SET_DRIVERS
    drivers: Array<DriverType>
}

export type setDriversPagePaginationActionType = {
    type: typeof SET_DRIVERS_PAGINATION
}

export const setDrivers = (drivers: Array<DriverType>): setDriversActionType => ({type: SET_DRIVERS, drivers});

export const setDriversPagination = (): setDriversPagePaginationActionType => (
    {type: SET_DRIVERS_PAGINATION}
);

export const requestDrivers = (offset: number, limit: number) => {
    return async (dispatch: Dispatch<DriversListActionsType>, getState: RootStateType) => {
        let data = await driversAPI.getDrivers(offset, limit)
        dispatch(setDrivers(data))
    }
}

export type DriversListActionsType = setDriversActionType | setDriversPagePaginationActionType
