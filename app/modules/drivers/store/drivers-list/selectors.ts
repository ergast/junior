import {createSelector} from "reselect";
import {driversState} from "../selectors";

export const driversListStateSelector = createSelector(
    driversState,
    (state) => state.driversReducer
)

export const driversSelector = createSelector(
    driversListStateSelector,
    (state) => state.drivers
)

export const pagePaginationSelector = createSelector(
    driversListStateSelector,
    (state) => state.pagination
)