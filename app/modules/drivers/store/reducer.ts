import {combineReducers} from "redux";
import {driversReducer} from "./drivers-list/reducer";
import {driverDetailReducer} from "./driver-detail/reducer";


export const driversRootReducer = combineReducers({
    driversReducer,
    driverDetailReducer
})

export type DriversListRootStateType = ReturnType<typeof driversRootReducer>
