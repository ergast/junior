import {AxiosResponse} from "axios";
import instance from "../../../core/API";
import {DriverType} from "../interfaces/driverType";
import { APIResponseList } from "../../../core/interfaces/APIResponseList";
import { APIResponse } from "../../../core/interfaces/APIResponse";

export interface APIResponseDrivers extends APIResponseList {
    DriverTable: {
        circuitId: string,
        constructorId: string,
        Drivers: Array<DriverType>
    }
}

export const driversAPI = {
    getDrivers(offset: number, limit: number) {
        return instance.get(`drivers.json?limit=${limit}&offset=${offset}`)
            .then((response: AxiosResponse<APIResponse<APIResponseDrivers>>) => response.data.MRData.DriverTable.Drivers)
    },
    getDriver(id: string) {
        return instance.get(`drivers/${id}.json`)
            .then((response: AxiosResponse<APIResponse<APIResponseDrivers>>) => response.data.MRData.DriverTable.Drivers[0])
    }

}
