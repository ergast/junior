import {combineReducers} from "redux";
import {driversRootReducer} from "../drivers/store/reducer";
import {racesRootReducer} from "../races/store/reducer";

export const rootReducer = combineReducers({
    driversRootReducer,
    racesRootReducer
});

export type RootStateType = ReturnType<typeof rootReducer>
