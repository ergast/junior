export interface APIResponseList {
  xmlns: string
  series: string
  limit: string
  offset: string
  total: string
}