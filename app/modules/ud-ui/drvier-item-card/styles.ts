import styled from 'styled-components/native'

export const ItemCard = styled.View`
  flex-basis: 20%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  align-items: center;
  padding: 5px 0;
  margin: 10px;
  border: 1px solid black;
  border-radius: 12px;
  box-shadow: 0 0 10px rgba(0,0,0,0.5)
`;
