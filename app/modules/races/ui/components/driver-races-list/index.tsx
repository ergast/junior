import {ScrollView} from "react-native";
import React from "react";
import {RaceType} from "../../../domain/interfaces/racesTypes";
import {DriverRacesListItem} from "../driver-races-list-item";

export const DriverRacesList = (props: { races: RaceType[] }) => {
    return (
        <ScrollView>
            {props.races.map(
                race => <DriverRacesListItem key={race.url} race={race}/>
            )}
        </ScrollView>
    );
}

