import {StyleSheet, Text, TouchableHighlight, View} from "react-native";
import React from "react";
import {RaceType} from "../../../domain/interfaces/racesTypes";
import {ItemCard} from "../../../../ud-ui/drvier-item-card/styles";


type OwnProps = {
    race: RaceType,
};

type Props = OwnProps

export const DriverRacesListItem: React.FC<Props> = ({race}) => {
    return (
        <ItemCard>
            <View style={styles.row}>
                <Text style={styles.title}>
                    {race.raceName}
                </Text>
            </View>
            <View style={styles.row}>
                <Text style={{margin: 5}}>
                    Season: {race.season}
                </Text>
                <Text style={{margin: 5}}>
                    {race.date} {race.time}
                </Text>
            </View>
            <View style={styles.row}>
                <Text style={{margin: 5}}>
                    Location: {race.Circuit.Location.country}
                </Text>
            </View>
            <View style={styles.row}>
                <Text style={{margin: 5}}>
                    Circuit name: {race.Circuit.circuitName}
                </Text>
            </View>
        </ItemCard>
    );
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    title: {
        fontSize: 30,
        justifyContent: "center",
        margin: 10
    },
});
