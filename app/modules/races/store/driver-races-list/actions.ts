import {RaceType} from "../../domain/interfaces/racesTypes";
import { RootStateType } from "../../../store/reducer";
import {Dispatch} from "redux";
import { racesAPI } from "../../domain/API/racesAPI";

export const SET_DRIVER_RACES = 'SET_DRIVER_RACES';
export const SET_DRIVER_RACES_PAGINATION = 'SET_DRIVER_RACES_PAGINATION';


export type DriverRacesPagePagination = {
    offset: number,
    limit: number
}

export type setDriverRacesActionType = {
    type: typeof SET_DRIVER_RACES
    races: Array<RaceType>
}

export type setDriverRacesPagePaginationActionType = {
    type: typeof SET_DRIVER_RACES_PAGINATION
}

export const setDriverRaces = (races: Array<RaceType>): setDriverRacesActionType => ({type: SET_DRIVER_RACES, races});

export const setDriverRacesPagePagination = (): setDriverRacesPagePaginationActionType => (
    {type: SET_DRIVER_RACES_PAGINATION}
);

export const requestDriverRaces = (driver_id: string, offset: number, limit: number) => {
    return async (dispatch: Dispatch<DriverRacesListActionsType>, getState: RootStateType) => {
        let data = await racesAPI.getDriverRaces(driver_id, offset, limit)
        dispatch(setDriverRaces(data))
    }
}

export type DriverRacesListActionsType = setDriverRacesActionType | setDriverRacesPagePaginationActionType
