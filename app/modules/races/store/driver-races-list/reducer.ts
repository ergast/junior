import {RaceType} from "../../domain/interfaces/racesTypes";
import {
    DriverRacesListActionsType,
    DriverRacesPagePagination,
    SET_DRIVER_RACES,
    SET_DRIVER_RACES_PAGINATION,
} from "./actions";

export const driverRacesInitialState = {
    races: [] as RaceType[],
    pagination: {
        offset: 0, limit: 10
    } as DriverRacesPagePagination
}

type DriverRacesListInitialStateType = typeof driverRacesInitialState

export const driverRacesReducer = (
    state = driverRacesInitialState, action: DriverRacesListActionsType
): DriverRacesListInitialStateType => {
    switch (action.type) {
        case SET_DRIVER_RACES:
            return {
                ...state,
                races: [...state.races, ...action.races]
            }
        case SET_DRIVER_RACES_PAGINATION:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    offset: (state.pagination.offset + state.pagination.limit)
                }
            }
        default:
            return state
    }
};
