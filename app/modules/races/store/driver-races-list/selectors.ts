import {createSelector} from "reselect";
import {driverRacesState} from "../selectors";

export const  driverRacesStateListSelector = createSelector(
    driverRacesState,
    (state) => state.driverRacesReducer
);

export const racesSelector = createSelector(
    driverRacesStateListSelector,
    (state) => state.races
);

export const pagePaginationSelector = createSelector(
    driverRacesStateListSelector,
    (state) => state.pagination
);