import { AxiosResponse } from "axios";
import instance from "../../../core/API";
import { RaceType } from "../interfaces/racesTypes";
import { APIResponseList } from "../../../core/interfaces/APIResponseList";
import { APIResponse } from "../../../core/interfaces/APIResponse";

export interface APIResponseRaces extends APIResponseList {
    RaceTable: {
        season: string
        Races: RaceType[]
    }
}

export const racesAPI = {
    getDriverRaces(id: string, offset: number, limit: number) {
        return instance.get(`drivers/${id}/races.json?limit=${limit}&offset=${offset}`)
            .then((response: AxiosResponse<APIResponse<APIResponseRaces>>) => response.data.MRData.RaceTable.Races)
    },
}
