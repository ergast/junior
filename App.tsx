import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from "react-redux";
import Drivers from "./app/modules/drivers/ui/pages/index";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from "redux-thunk"
import {rootReducer} from "./app/modules/store/reducer";
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from "@react-navigation/native";
import DriverDetail from "./app/modules/drivers/ui/pages/detail/index"
import DriverRaces from "./app/modules/races/ui/pages/index/index"
import {StyleSheet, View} from "react-native";

export type RootStackParamList = {
    DRIVERS: undefined;
    DRIVER: { id: string };
    DRIVER_RACES: { driver_id: string };
}
const RootStack = createStackNavigator<RootStackParamList>();

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export default function App() {
    return (
        <Provider store={store}>
            <View style={styles.container}>
                <NavigationContainer>
                    <RootStack.Navigator initialRouteName="DRIVERS">

                        <RootStack.Screen name="DRIVERS" component={Drivers}/>
                        <RootStack.Screen name="DRIVER" component={DriverDetail}/>
                        <RootStack.Screen name="DRIVER_RACES" component={DriverRaces}/>

                    </RootStack.Navigator>
                </NavigationContainer>
            </View>
        </Provider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f0f4da',
        justifyContent: "flex-start",
    },
});

